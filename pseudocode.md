## Pseudocode

<span class="hidden-text">https://oer-informatik.de/pseudocode</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/112027742368445364/</span>


> **tl/dr;** _(ca. 6 min Lesezeit): Die  Dokumentation oder der Entwurf von Algorithmen soll häufig unabhängig von einer spezifischen Programmiersprache erfolgen. Eine Möglichkeit ist es,  Pseudocode zu verwenden. Drei verbreitete Pseudocode-Notationen werden hier dargestellt: JANA, Pseudo-Pascal und eine deutsche Übersetzung von Pseudo-Pascal. Für alle Varianten werden Schlüsselwörter und Konstrukte aufgezeigt, um Algorithmen mit Pseudocode beschreiben zu können._

### Darstellungsformen von Algorithmen

Um Algorithmen allgemeinverständlich auszudrücken, ohne den Adressatenkreis auf eine Programmiersprache einzugrenzen, werden häufig Programmiersprachen-unabhängige Ausdrucksformen gewählt. Neben

* dem Programmablaufplan (PAP, DIN 66001) [(siehe Übersicht der Programmentwurfstechniken)](https://oer-informatik.de/programmentwurfstechniken),

* dem Nassi-Shneiderman-Diagramm ("Struktogramm", DIN 66261) [(siehe Übersicht der Programmentwurfstechniken)](https://oer-informatik.de/programmentwurfstechniken)

* oder dem [UML-Aktivitäts-Diagramm (siehe gesonderter Artikel)](https://oer-informatik.de/uml-aktivitaetsdiagramm)

ist dies v.a. auch eine natürlichsprachlich vereinfachte und syntaxfreie Form des Programmcodes - **der Pseudocode**.

### Geläufige Pseudocode-Notationen

Für Pseudocode gibt es kein festes einheitliches Regelwerk, vielmehr muss eine für den Adressatenkreis passende Variante gewählt werden: Häufig wird eine geläufige Programmiersprache vereinfacht. Beispielhaft werden hier drei Varianten vorgestellt, die häufig eingesetzt werden:

* _JANA_: Insbesondere für Entwickler*innen mit Java/C/C++/C#-Kenntnissen leicht verständliche Notation (Java-Based Abstract Notation for Algorithms)

* _Pseudo-Pascal_: An die Programmiersprache Pascal angelehnte Variante (v.a. in älteren Lehrwerken verbreitet)

* _deutsches Pseudo-Pascal_ (eigene Namensschöpfung): eine eingedeutschte Pseudo-Pascal-Variante, die vor allem im Bereich der Fachinformatiker*innen-Ausbildung verwendet wird (da die IHK sie in den Prüfungen und Beispiellösungen verwendet).

Alle diese Varianten lassen sich mit etwas Übung gleichwertig nutzen. Ungeübt ist es empfehlenswert, nahe an einer vertrauten Programmiersprache zu bleiben. Man sollte dann allerdings aufpassen, dass man auf den Einsatz von Sprachkonstrukten verzichtet, die es nur in bestimmten Programmiersprachen gibt (z.B. Lambda-Ausdrücke, Annotations, Dekorator, bestimmte Iterations- Bedingungsabkürzungen). 

Für angehende Fachinformatiker*innen kann es sinnvoll sein, sich direkt mit dem _deutschen Pseudo-Pascal_ vertraut zu machen: Es wird häufig in den Musterlösungen der IHK-Prüfung verwendet und wurde zuletzt auch einmal in der Notationshilfe zur Prüfung abgedruckt.


### Wesentliche Eigenschaften von Pseudocode

Wichtig bei der Erstellung von Pseudocode ist:

- **Verständlichkeit** des Pseudocodes ist wichtiger als starre Konventionen!

- **Unnötige Details vermeiden**: das Offensichtliche kann als bekannt vorausgesetzt werden!

- **Im Kontext bleiben**: Wer ist Adressat und in welcher Problemdomäne befinde ich mich? Wem beantworte ich mit dem Pseudocode welche Frage?

- **Stimmige Abstraktionsgrade wählen**: Baut der Abstraktionsgrad meiner Operationen auf den Abstraktionsgrad des umgebenden Moduls auf? Das _Single Layer of Abstraction (SLA)_-Prinzip gilt auch für Pseudocode!

### Beispiele für Pseudocode

#### Definition von Blöcken (_Scopes_ / _Suites_)

Programmblöcke definieren den Gültigkeitsbereich von lokalen Variablen und markieren das Ende von Schleifen und Bedingungen. Wie in Java üblich erfolgt die Blockdefinition bei _JANA_ in geschweiften Klammern. In den Pascal-Pseudocodes wird jedes Schlüsselwort mit `end` und dem Schlüsselwort beendet. Alternativ ist auch die Einrückung als Blockdefinition möglich (Python-Stil). Dies ist jedoch insbesondere bei handschriftlicher Notation ggf. fehleranfällig und schwer zu korrigieren.

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- |
|`{`<br>&nbsp;&nbsp;`Anweisung`<br>`}`    |`for (...)`<br>&nbsp;&nbsp;`Anweisung`<br>`end for` | `WENN (...)`<br>&nbsp;&nbsp;`Anweisung`<br>`ENDE WENN` |


#### Deklaration von Datentypen

Variablen, die nicht als Parameter übergeben werden (oder als global gültig definiert wurden), sollten in irgendeiner Form deklariert werden. Abhängig von der gewählten Pseudocode-Art wird Datentyp oder Name zuerst genannt. Bei _Pseudo-Pascal_ erfolgt die Typdeklaration mit dem Doppelpunkt `:`.

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- |
| `int i` |`i: int`| `i: GANZZAHL` |  
| `int i := 0` |`i: int := 0`| `i: GANZZAHL := 0 `|  
| `Object o` |`o: Object`| `o: OBJEKT`|

Die implizite Deklaration und Initialisierung sollte bei _Pseudo-Pascal_ vermieden werden, da sie unübersichtlich wird. Bei _JANA_ ist empfehlenswert für Zuweisungen mit `:=` zu kennzeichnen (nach Standard würde `=` reichen, ist aber weniger deutlich).

Es sollten keine Programmiersprachen-spezifischen Datentypen verwendet werden. Üblich sind etwa folgende Standardtypen:

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ | Beispiel|
| :-------- | :-------- | :-------- | :-------- |
| int |integer| GANZZAHL | -1, 0, 1, 2 |
| float |real| GLEITKOMMAZAHL| 0.0|
| char |char| ZEICHEN | 'a', 'b', 'c'|
| boolean |boolean| WAHRHEITSWERT| true, false
| String |String| ZEICHENKETTE| "Hallo Welt"|

#### Operatoren

| Operator | Erläuterung |
| :-------- | :-------- |
| `+`,`-`,`/`,`*`    | Mathematische Operatoren |
| `&`    | sollte kommentiert werden, ob es sich um Bitoperation oder Konkatenierung handelt |
| `:=`    | Zur besseren Unterscheidung von Zuweisungsoperator und Vergleichsoperator sollte im Pseudocode ":=" für Zuweisungen verwendet werden (`i := i+1`) (in JANA gilt eigentlich `=` als Zuweisung)|
| `<`, `>`, `==`, `!=`| Vergleichsoperatoren (`Ergebnis == true`)|
| `:`| Bei _Pseudo-Pascal_: Deklaration von Datentypen (`text :  String`); sollte _nicht_ für Division verwendet werden!|
| `AND`, `OR`, `NOT` | Logische Operatoren. Auch Deutsch möglich. `&` bzw. `|` sollte wegen Doppeldeutigkeit vermieden oder per Kommentar erläutert werden.  (`i>5 AND b==2`) |

### Selektion / bedingte Anweisung

#### Einfache Bedingung

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- | 
| `if (...){`<br>&nbsp;&nbsp;`Anweisung`<br>`}else if (...){`<br>&nbsp;&nbsp;`Anweisung`<br>`}else{`<br>&nbsp;&nbsp;`Anweisung`<br>`}` |`if (...) then`<br>&nbsp;&nbsp;`Anweisung`<br>`else if (...)`<br>&nbsp;&nbsp;`Anweisung`<br>`else`<br>&nbsp;&nbsp;`Anweisung`<br>`end if` |`WENN (...) DANN`<br>&nbsp;&nbsp;`Anweisung`<br>`SONST WENN (...) DANN`<br>&nbsp;&nbsp;`Anweisung`<br>`SONST`<br>&nbsp;&nbsp;`Anweisung`<br>`ENDE WENN` |

#### Bedingung mit Mehrfachauswahl

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- | 
| `switch (variable) {`<br>&nbsp;&nbsp;`case 1: {Anweisung}`<br>&nbsp;&nbsp;`case >=2: {Anweisung}`<br>&nbsp;&nbsp;`default: {Anweisung}`<br>`}` |`case  variable of `<br>&nbsp;&nbsp;`1: Anweisung`<br>&nbsp;&nbsp;` >=2: Anweisung`<br>&nbsp;&nbsp;`else Anweisung`<br>`end case`  |`WERT VON  variable ENTSPRICHT `<br>&nbsp;&nbsp;`1: Anweisung`<br>&nbsp;&nbsp;` >=2: Anweisung`<br>&nbsp;&nbsp;`SONST Anweisung`<br>`ENDE WERT VON`  |

In JANA sind im Gegensatz zu Java keine `break`-Statements nötig, zudem können auch booleansche Ausdrücke (`>=2`) genutzt werden.


### Iterationen / Wiederholungsstrukturen

#### Kopfgesteuerte Schleifen

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- | 
| `while (...){`<br>&nbsp;&nbsp;`Anweisung`<br>`}`    |`while (...) do`<br>&nbsp;&nbsp;`Anweisung`<br>`end while` | `SOLANGE (...)`<br>&nbsp;&nbsp;`Anweisung`<br>`ENDE SOLANGE` |


#### Fussgesteuerte Schleifen

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- | 
| `do {`<br>&nbsp;&nbsp;`Anweisung`<br>`} while (...)`    |`repeat`<br>&nbsp;&nbsp;`Anweisung`<br>`until (...)` | `WIEDERHOLE`<br>&nbsp;&nbsp;`Anweisung`<br>`SOLANGE (...)` |

#### Zählergesteuerte Schleifen

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- | 
| `for (...) {`<br>&nbsp;&nbsp;`Anweisung`<br>`}`    |`for (...)`<br>&nbsp;&nbsp;`Anweisung`<br>`end for` | `ZÄHLE i VON 0 BIS 7`<br>&nbsp;&nbsp;`Anweisung`<br>`ENDE ZÄHLE` |

Die zur iterierende Menge kann relativ frei eingegeben werden, solange sie eindeutig definiert wird. Denkbar ist z.B.:

- `for (int i = 1..10)`

- `for (int i; i<=10; i++)`

- `for (int i = 1, 2, 3, .., 10)`

Iterationen durch Mengen/Listen können auch in Kurzschreibweise erfolgen:

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- | 
| `foreach (String txt in txtList) {`<br>&nbsp;&nbsp;`Anweisung`<br>`}`    |`foreach element in list`<br>&nbsp;&nbsp;`Anweisung`<br>`next element` | `FÜR JEDES element VON liste`<br>&nbsp;&nbsp;`Anweisung`<br>`NÄCHSTES element` |

### Methodendeklaratione

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- | 
| `String halloWelt(String text){`<br>&nbsp;&nbsp;`return "Hallo " & text`<br>`}` |`FUNCTION halloWelt(text: String) : String`<br>&nbsp;&nbsp;`Anweisung`<br>`END halloWelt()` | `FUNKTION halloWelt(text: String) : String`<br>&nbsp;&nbsp;`RÜCKGABE "Hallo " & text`<br>`ENDE halloWelt()` |

Aufgerufen wird die obige Methoden einheitlich im Pseudocode mit `halloWelt("Herbert")`.

### Arrays und Collections

Arrays und Objektsammlungen können im Pseudocode ebenfalls eingesetzt werden, wobei auch hier auf sprachspezifische Konstrukte wo immer möglich verzichtet werden sollte (z.B. Dictionaries, Maps, Sets).

| _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- | 
| `int zahlenfeld[]` |`zahlenFeld = array [] of integer` | `zahlenFeld = eindimensionales Ganzzahlen-Array` |
| `int zahlenfeld[1..100]` |`zahlenFeld = array [1..100] of integer` | `zahlenFeld = eindimensionales Ganzzahlen-Array mit 100 Elementen` |
| `zahlenfeld[n, m]` |`zahlenFeld = array [1..n, 1..m] of integer` | `zahlenFeld = zweidimensionales Ganzzahlen-Array` |
| `zahlen2D[].length` |`zahlen2D[].length` | `zahlen2D[].LÄNGE` |


Zu beachten ist, dass bei Java/C der Array-Index üblicherweise mit 0 anfängt, bei Pascal mit 1: es sollte daher eindeutig beschrieben werden, welche Variante man wählt (obige Beispiele haben daher unterschiedlich viele Elemente!).

### Objekte / Klassen

In selteneren Fällen wird Pseudocode auch zur Darstellung objektorientierter Programmblöcke verwendet. Häufig ist hier jedoch die direkte Implementierung in einer Sprache oder die Darstellung über UML zielführender.

| | _JANA_ | _Pseudo-Pascal_ | _deutsches Pseudo-Pascal_ |
| :-------- | :-------- | :-------- | :-------- | 
| Kopf| `class Auto{`<br>&nbsp;&nbsp;`...`<br>`}` |`class Auto `<br>&nbsp;&nbsp;`...`<br>`end class` | `KLASSE Auto`<br>&nbsp;&nbsp;`...`<br>`ENDE KLASSE Auto` |
| Typdeklartion<br>Instanziierung| `Auto a := new Auto()` |`a : Auto`<br>`a := new Auto()`  | `a : Auto`<br>`a := INSTANZ VON Auto()` |
| Konstruktor| `public Auto(){`<br>&nbsp;&nbsp;`Anweisung`<br>`}` |`public CONSTRUCTOR Auto() `<br>&nbsp;&nbsp;`Anweisung`<br>`END Auto()` | `ÖFFENTLICH KONSTRUKTOR Auto()`<br>&nbsp;&nbsp;`Anweisung`<br>`ENDE Auto()` |
| Getter| `public String getName(){`<br>&nbsp;&nbsp;`return name`<br>`}` |`public Function getName(): String `<br>&nbsp;&nbsp;`Anweisung`<br>`END getName()` | `ÖFFENTLICH FUNKTION getName():String`<br>&nbsp;&nbsp;`RÜCKGABE name`<br>`ENDE getName()` |
| Setter| `public void setName(String name){`<br>&nbsp;&nbsp;`this.name := name`<br>`}` |`public FUNCTION setName(name: String) :void`<br>&nbsp;&nbsp;`this.name := name`<br>`END setName()` | `ÖFFENTLICH FUNKTION setName(name: String) :void`<br>&nbsp;&nbsp;`this.name := name`<br>`ENDE setName()` |

### Kommentare

Kommentare sind in den verbreitesten Notationen möglich, z.B.:

`// Kommentar`

`/* Kommentar */`

`# Kommentar`

### Links und weitere Informationen

- [JANA-Stanard der JKU Linz](http://ssw.jku.at/Teaching/Lectures/Algo/Jana.pdf)
