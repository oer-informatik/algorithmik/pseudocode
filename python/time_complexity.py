#from datetime import datetime
#from datetime import timedelta
from timeit import default_timer as timer
import numpy as np
import random
import time
#pip install matplotlib seaborn
import matplotlib.pyplot as pyplot
import seaborn as sns

def bubbleSort(arr):
    n = len(arr)
 
    # Traverse through all array elements
    for i in range(n-1):
    # range(n) also work but outer loop will repeat one time more than needed.
        vertauscht = False
        # Last i elements are already in place
        for j in range(0, n-i-1):
 
            # traverse the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if arr[j] > arr[j + 1] :
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
                vertauscht = True
                
        if vertauscht == False:
            break
                

def get_first(testlist):
    print(testlist[0])

def get_sum(testlist):
    sum =0
    for i in testlist:
        sum = sum + i
    return sum

def find_duplicates(testlist):
    duplicates = 0
    for i in testlist:
      for j in testlist:
          if i == j:
            duplicates =+ 1
    return duplicates

def binary_search(testlist):
    lower_bound_index = 0
    upper_bound_index = len(testlist) - 1
    
    searched_number = int(len(testlist)-2)
    #print("Liste:" + str(testlist))
    
    while (lower_bound_index <= upper_bound_index):
 
        mid_index = (upper_bound_index + lower_bound_index) // 2
        #print("von "+str(lower_bound_index)+" bis "+str(upper_bound_index)+" gesucht: "+str(searched_number))
 
        if testlist[mid_index] < searched_number:
            lower_bound_index = mid_index + 1
 
        elif testlist[mid_index] > searched_number:
            upper_bound_index = mid_index - 1
 
        else:
            return mid_index
    return -1


def messe_laufzeit(beobachtete_funktion, array):
    start = timer() #time.perf_counter() #datetime.now()

    beobachtete_funktion(array)

    ende = timer() #time.perf_counter() #datetime.now()
    laufzeit = ende-start
    dauer = (ende-start)
    print(" "+str(1000*dauer)+ "ms")
    return dauer*1000

elementeanzahlen=[]
bestcase_time=[]
worstcase_time=[]
averagecase_time=[]


def messung_fuer_anzahl_an_elementen(beobachtete_funktion,
                                     elementeanzahl,
                                     averagecase_list=None,
                                     worstcase_list=None,
                                     bestcase_list=None):   
    print("Test mit " + str(elementeanzahl) + " Elementen")

    bestcase_testlist=[]
    worstcase_testlist=[]
    averagecase_testlist=[]
    
    for i in range(0, elementeanzahl-1):
        if (bestcase_list is not None):
            bestcase_testlist.append(bestcase_list[i])
        if (averagecase_list is not None):
            averagecase_testlist.append(averagecase_list[i])
        if (worstcase_list is not None):
            worstcase_testlist.append(worstcase_list[i])
    elementeanzahlen.append(elementeanzahl)
    
    if (bestcase_list is not None):
        bestcase_time.append(messe_laufzeit(beobachtete_funktion,bestcase_testlist))
    if (worstcase_list is not None):
         worstcase_time.append(messe_laufzeit(beobachtete_funktion,worstcase_testlist))
    if (averagecase_list is not None):
         averagecase_time.append(messe_laufzeit(beobachtete_funktion,averagecase_testlist))

   
def fuehre_laufzeitmessung_durch(beobachtete_funktion, titel='Zeitliche Komplexität von Algorithmen', dateiname='plot', averagecase_list=[], bestcase_list = None, worstcase_list=None, maximale_anzahl_elemente=200, anzahl_aufrufe=50):    

    # Fall für eine List [] übergeben wurde, wird diese mit Standardwerten gefüllt
    if (bestcase_list is not None) and (len(bestcase_list)== 0):
        bestcase_list = range(0,maximale_anzahl_elemente)
    if (averagecase_list is not None) and (len(averagecase_list)== 0):
        averagecase_list = np.random.randint(0,maximale_anzahl_elemente, maximale_anzahl_elemente)
    if (worstcase_list is not None) and (len(worstcase_list)== 0):
        worstcase_list = range(maximale_anzahl_elemente,0,-1)

    # Es wird die Funktion _anzahl_aufrufe_-mal aufgerufen mit gleichgroßen
    # Sprüngen in der Anzahl der Elemente - maximal jedoch maximale_anzahl_elemente
    for anzahl_elemente_dieser_durchlauf in range(int(maximale_anzahl_elemente/anzahl_aufrufe), maximale_anzahl_elemente, int(maximale_anzahl_elemente/anzahl_aufrufe)) :
    #for anzahl_elemente_dieser_durchlauf in (int(2**i) for i in range(-1,int(1+np.log(maximale_anzahl_elemente)/np.log(2)))) :
         messung_fuer_anzahl_an_elementen(beobachtete_funktion, anzahl_elemente_dieser_durchlauf, averagecase_list = averagecase_list, worstcase_list = worstcase_list, bestcase_list = bestcase_list)

    # Sofern für best / average / worst-case Daten vorliegen wird der entsprechende Graph geplottet
    if (len(worstcase_time) > 0):
        pyplot.plot(elementeanzahlen, worstcase_time, color="red", linewidth=2.5, linestyle="-", label='Worst-Case')
    if (len(averagecase_time) >0):
        pyplot.plot(elementeanzahlen, averagecase_time, color="blue", linewidth=2.5, linestyle="-", label='Average')
    if (len(bestcase_time) > 0):
        pyplot.plot(elementeanzahlen, bestcase_time, color="green", linewidth=2.5, linestyle="-", label='Best-Case')
    pyplot.xlabel('Anzahl Elemente')
    pyplot.axis( [0, maximale_anzahl_elemente,0,None])
    pyplot.ylabel('Verarbeitungszeit in ms')
    pyplot.title(titel)
    pyplot.grid(True)
    pyplot.legend(loc='upper left', frameon=True)
    # Zeit und Datum
    Datum = time.strftime("%Y-%m-%d")
    Zeit = time.strftime("%H%M")
    pyplot.savefig(dateiname+'_'+Datum+'_'+Zeit+'.png')
    pyplot.show()

    
#testbestworstsort(bubbleSort, maximale_anzahl_elemente=200, anzahl_aufrufe=50) 
#testbestworstsort(get_sum, maximale_anzahl_elemente=2000, anzahl_aufrufe=200) 
#fuehre_laufzeitmessung_durch(get_first, maximale_anzahl_elemente=10000, anzahl_aufrufe=20) 
#fuehre_laufzeitmessung_durch(get_sum,  titel="Summiere", dateiname="summe",maximale_anzahl_elemente=10000, anzahl_aufrufe=20) 
fuehre_laufzeitmessung_durch(bubbleSort, titel="Bubblesort",dateiname="bubblesort", maximale_anzahl_elemente=1000, anzahl_aufrufe=80, bestcase_list = [], worstcase_list = []) 

#fuehre_laufzeitmessung_durch(get_first, titel="Zeitliche Komplexität von getFirst() über die Anzahl der Elemente", dateiname="getFirst", maximale_anzahl_elemente=10000, anzahl_aufrufe=400) 
#fuehre_laufzeitmessung_durch(get_sum, titel="Zeitliche Komplexität von getSum() über die Anzahl der Elemente", dateiname="getFirst", maximale_anzahl_elemente=10000, anzahl_aufrufe=400) 
#fuehre_laufzeitmessung_durch(bubbleSort, titel="Zeitliche Komplexität von BubbleSort über die Anzahl der Elemente",dateiname="bubblesort", maximale_anzahl_elemente=5000, anzahl_aufrufe=160, bestcase_list = [], worstcase_list = []) 
#fuehre_laufzeitmessung_durch(bubbleSort, titel="Zeitliche Komplexität von BubbleSort über die Anzahl der Elemente",dateiname="bubblesort", maximale_anzahl_elemente=5000, anzahl_aufrufe=160) 
#fuehre_laufzeitmessung_durch(find_duplicates, titel="Zeitliche Komplexität von find_duplicates() über die Anzahl der Elemente",dateiname="find_duplicates", maximale_anzahl_elemente=5000, anzahl_aufrufe=180) 
#fuehre_laufzeitmessung_durch(binary_search, titel="Zeitliche Komplexität der binären Suche über die Anzahl der Elemente",dateiname="binary_search", maximale_anzahl_elemente=1600, anzahl_aufrufe=1600, averagecase_list = range(0,1600)) 



